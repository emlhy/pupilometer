package com.example.demonpilot.pupilometer.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by Demon Pilot on 2015/10/2.
 */
public class CropSquareView extends View{
    public static final int BORDER_DISTANCE = 100;
    private final float LINE_BORDER_WIDTH = 2f;
    private final int LINE_COLOR = Color.WHITE;
    private final Paint paint = new Paint();
    private Paint circle;
    private DashPathEffect dashPath;
    private Point size;
    //public static float translateX;
    //public static float translateY;

    public CropSquareView(Context context) {
        super(context);
    }

    public CropSquareView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = getWidth();
        int height = getHeight();

        paint.setColor(Color.parseColor("#76000000"));

        boolean isHorizontal = false;
        if(width > height) {
            isHorizontal = true;
        }

        int outLeft = 0;
        int outTop = 0;
        int outRight = width;
        int outBottom = height;
        int borderlength = isHorizontal ? (height - BORDER_DISTANCE * 2) : (width - BORDER_DISTANCE * 2);
        int inLeft = isHorizontal ? ((width - borderlength) / 2) : BORDER_DISTANCE;
        int inTop = isHorizontal ? BORDER_DISTANCE : ((height - borderlength) / 2);
        int inRight = isHorizontal ? (inLeft + borderlength) : borderlength + BORDER_DISTANCE;
        int inBottom = isHorizontal ? borderlength + BORDER_DISTANCE : (inTop + borderlength);

        canvas.drawRect(outLeft, outTop, outRight, inTop, paint);
        canvas.drawRect(outLeft, inBottom, outRight, outBottom, paint);
        canvas.drawRect(outLeft, inTop, inLeft, inBottom, paint);
        canvas.drawRect(inRight, inTop, outRight, inBottom, paint);

        paint.setColor(LINE_COLOR);
        paint.setStrokeWidth(LINE_BORDER_WIDTH);

        canvas.drawLine(inLeft, inTop, inLeft, inBottom, paint);
        canvas.drawLine(inRight, inTop, inRight, inBottom, paint);
        canvas.drawLine(inLeft, inTop, inRight, inTop, paint);
        canvas.drawLine(inLeft, inBottom, inRight, inBottom, paint);

        dashPath = new DashPathEffect(new float[]{4, 4}, 1);
        circle = new Paint();
        size = new Point();
        circle.setAntiAlias(true);
        circle.setARGB(255, 255, 0, 0);
        circle.setPathEffect(dashPath);
        circle.setStyle(Paint.Style.STROKE);
        circle.setStrokeWidth(6.0f);
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getSize(size);
        //translateX = size.x / 2;
        //translateY = inTop + borderlength / 2;
        canvas.drawPoint(size.x / 2, inTop + borderlength / 2, circle);
        canvas.drawCircle(size.x / 2, inTop + borderlength / 2, borderlength / 2, circle);
    }
}
