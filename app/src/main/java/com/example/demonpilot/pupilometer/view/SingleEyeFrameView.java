package com.example.demonpilot.pupilometer.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.example.demonpilot.pupilometer.fragment.CameraFragment;
import com.example.demonpilot.pupilometer.fragment.SingleEyeFragment;

/**
 * Created by Demon Pilot on 2015/12/3.
 */
public class SingleEyeFrameView extends View {
    private Paint line;
    private Path top;
    private Path bottom;
    private DashPathEffect dashPath;
    public static float eyeX;
    public static float eyeY;

    public SingleEyeFrameView(Context context) {
        super(context);
    }

    public SingleEyeFrameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SingleEyeFrameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        dashPath = new DashPathEffect(new float[]{4, 4}, 1);
        float topBorder = SingleEyeFragment.position[1] - CameraFragment.topOffset - dpToPx(20);
        float centerX = SingleEyeFragment.width / 2;
        float centerY = topBorder + SingleEyeFragment.height / 2;
        float eyeWidth = SingleEyeFragment.width / 3;
        top = new Path();
        bottom = new Path();
        eyeX = centerX;
        eyeY = centerY - dpToPx(45);
        top.moveTo(centerX - eyeWidth / 2, centerY - dpToPx(55));
        top.cubicTo(centerX - eyeWidth / 2 + eyeWidth / 3, centerY - dpToPx(90), centerX - eyeWidth / 2 + eyeWidth * 2 / 3, centerY - dpToPx(90), centerX + eyeWidth / 2, centerY - dpToPx(55));
        bottom.moveTo(centerX - eyeWidth / 2, centerY - dpToPx(35));
        bottom.cubicTo(centerX - eyeWidth / 2 + eyeWidth / 3, centerY, centerX - eyeWidth / 2 + eyeWidth * 2 / 3, centerY, centerX + eyeWidth / 2, centerY - dpToPx(35));
        line = new Paint();
        line.setAntiAlias(true);
        line.setARGB(255, 255, 0, 0);
        line.setPathEffect(dashPath);
        line.setStyle(Paint.Style.STROKE);
        line.setStrokeWidth(5.0f);
        canvas.drawPath(top, line);
        canvas.drawPath(bottom, line);
    }

    public float dpToPx(float dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        float px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
}
