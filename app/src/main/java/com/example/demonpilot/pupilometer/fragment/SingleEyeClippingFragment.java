package com.example.demonpilot.pupilometer.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;
import com.example.demonpilot.pupilometer.view.SingleEyeCropView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Demon Pilot on 2015/12/3.
 */
public class SingleEyeClippingFragment extends Fragment {
    private Button previous;
    private Button next;
    private TextView eyeText;
    private SingleEyeCropView singleEyeCropView;
    private String firstPath;
    private String secondPath;
    private String tempFirstPath;
    private String tempSecondPath;
    private Bitmap bitmap;
    private Bitmap cropBitmap;
    private File mediaStorageDir;

    public SingleEyeClippingFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_single_eye_clipping, container, false);
        firstPath = getArguments().getString("first_path");
        secondPath = getArguments().getString("second_path");
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        if(MainActivity.firstEye) {
            bitmap = BitmapFactory.decodeFile(firstPath, options);
        } else{
            bitmap = BitmapFactory.decodeFile(secondPath, options);
        }

        previous = (Button)rootView.findViewById(R.id.previous);
        next = (Button)rootView.findViewById(R.id.next);
        eyeText = (TextView)rootView.findViewById(R.id.eye_text);
        singleEyeCropView = (SingleEyeCropView)rootView.findViewById(R.id.crop_view);
        singleEyeCropView.setImageBitmap(bitmap);
        if(MainActivity.firstEye) {
            eyeText.setText("First eye");
        } else{
            eyeText.setText("Second eye");
        }
        MainActivity.dialog.dismiss();

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.firstEye = true;
                broadcastPopBackStack();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropBitmap = singleEyeCropView.crop();
                if (MainActivity.firstEye) {
                    saveImage(cropBitmap);
                    MainActivity.firstEye = false;
                    broadcastReopenSingleEye();
                } else {
                    saveImage(cropBitmap);
                    MainActivity.firstEye = true;
                    broadcastOpenSingleEyeRatio();
                }
            }
        });

        return rootView;
    }

    private void saveImage(Bitmap bitmap){
        File imageFile = getOutputMediaFile();
        if (imageFile == null) {
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getOutputMediaFile(){

        mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PupiloMeter");

        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }

        File mediaFile;
        if(MainActivity.firstEye) {
            tempFirstPath = firstPath.substring(0, firstPath.lastIndexOf(".")) + "_temp.jpg";
            tempSecondPath = secondPath;
            mediaFile = new File(tempFirstPath);
        } else{
            tempFirstPath = firstPath;
            tempSecondPath = secondPath.substring(0, secondPath.lastIndexOf(".")) + "_temp.jpg";
            mediaFile = new File(tempSecondPath);
        }
        return mediaFile;
    }

    private void broadcastOpenSingleEyeRatio() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_SINGLE_EYE_RATIO);
        intent.putExtra("first_path", tempFirstPath);
        intent.putExtra("second_path", tempSecondPath);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastReopenSingleEye(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_REOPEN_SINGLE_EYE);
        intent.putExtra("first_path", tempFirstPath);
        intent.putExtra("second_path", tempSecondPath);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastPopBackStack() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_POP_BACK_STACK);
        getActivity().sendBroadcast(intent);
    }
}
