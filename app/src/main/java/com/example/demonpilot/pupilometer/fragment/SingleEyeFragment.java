package com.example.demonpilot.pupilometer.fragment;

import android.app.ProgressDialog;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;
import com.example.demonpilot.pupilometer.view.SingleEyeFrameView;
import com.example.demonpilot.pupilometer.preview.SingleEyePreview;

/**
 * Created by Demon Pilot on 2015/12/2.
 */
public class SingleEyeFragment extends Fragment{
    private SingleEyePreview singleEyePreview;
    private FrameLayout preview;
    private FrameLayout frame;
    private Button buttonCapture;
    public static TextView flash;
    public static TextView switchCamera;
    private Camera.Parameters parameters;
    public static int height;
    public static int width;
    public static int[] position;
    public static int topOffset;
    public boolean hasMeasured;
    private SingleEyeFrameView singleEyeFrameView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_camera, container, false);
        final View view = rootView;
        hasMeasured = false;
        MainActivity.dialog = new ProgressDialog(getActivity());
        MainActivity.dialog.setMessage("Loading...");
        MainActivity.dialog.setCancelable(false);
        MainActivity.dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        preview = (FrameLayout) rootView.findViewById(R.id.camera_preview);
        frame = (FrameLayout) rootView.findViewById(R.id.frame);
        buttonCapture = (Button)rootView.findViewById(R.id.capture);
        flash = (TextView) rootView.findViewById(R.id.flash);
        switchCamera = (TextView) rootView.findViewById(R.id.switch_camera);

        buttonCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MainActivity.firstEye) {
                    MainActivity.dialog.show();
                }
                singleEyePreview.takePicture();
            }
        });

        ViewTreeObserver viewTreeobserver = frame.getViewTreeObserver();
        viewTreeobserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if(!hasMeasured) {
                    DisplayMetrics dm = new DisplayMetrics();
                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                    topOffset = dm.heightPixels - view.getMeasuredHeight();
                    height = frame.getHeight();
                    width = frame.getWidth();
                    position = new int[2];
                    frame.getLocationInWindow(position);
                    hasMeasured = true;
                }
                return true;
            }
        });

        singleEyeFrameView = new SingleEyeFrameView(getActivity());
        singleEyeFrameView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        frame.addView(singleEyeFrameView);

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        preview.removeView(singleEyePreview);
    }

    @Override
    public void onResume() {
        super.onResume();
        singleEyePreview = new SingleEyePreview(getActivity());
        singleEyePreview.setSurfaceTextureListener(singleEyePreview);
        preview.addView(singleEyePreview);
    }
}
