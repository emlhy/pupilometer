package com.example.demonpilot.pupilometer.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;

/**
 * Created by Demon Pilot on 2015/12/9.
 */
public class ScoreFragment extends Fragment {
    private TextView score;
    private Button done;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_score, container, false);
        score = (TextView) rootView.findViewById(R.id.score);
        done = (Button) rootView.findViewById(R.id.done);

        score.setText("Your score is: " + MainActivity.totalScore);
        score.setTextSize(25);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                broadcastOpenMain();
            }
        });
        return rootView;
    }

    private void broadcastOpenMain() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_MAIN);
        getActivity().sendBroadcast(intent);
    }
}
