package com.example.demonpilot.pupilometer.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.fragment.SingleEyeFragment;

/**
 * Created by Demon Pilot on 2015/12/3.
 */
public class SingleEyeCropView extends ImageView implements View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener {
    private static final int BORDER_DISTANCE = CropSquareView.BORDER_DISTANCE;

    public static final float DEFAULT_MAX_SCALE = 16.0f;
    public static final float DEFAULT_MID_SCALE = 8.0f;
    public static final float DEFAULT_MIN_SCALE = 1.0f / 8.0f;

    private float minScale = DEFAULT_MIN_SCALE;
    private float midScale = DEFAULT_MID_SCALE;
    private float maxScale = DEFAULT_MAX_SCALE;

    private MultiGestureDetector multiGestureDetector;
    private boolean initialising;

    private Matrix defaultMatrix = new Matrix();
    private Matrix dragMatrix = new Matrix();
    private Matrix finalMatrix = new Matrix();
    private final RectF displayRect = new RectF();
    private final float[] matrixValues = new float[9];

    private int borderLength;
    private float translateX;
    private float translateY;
    private float defaultScale;
    private float finalScale;
    private final float[] matrixV = new float[9];
    private float distance;
    public static float firstLeft;
    public static float firstRight;
    public static float firstTop;
    public static float firstBottom;
    public static float secondLeft;
    public static float secondRight;
    public static float secondTop;
    public static float secondBottom;

    public SingleEyeCropView(Context context, AttributeSet attrs) {
        super(context, attrs);
        super.setScaleType(ScaleType.MATRIX);
        setOnTouchListener(this);
        multiGestureDetector = new MultiGestureDetector(context);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getViewTreeObserver().removeGlobalOnLayoutListener(this);
    }

    @Override
    public void onGlobalLayout() {
        if(initialising) {
            return;
        }
        initBmpPosition();
    }

    private void initBmpPosition() {
        initialising = true;
        super.setScaleType(ScaleType.MATRIX);
        Drawable drawable = getDrawable();
        float screenScale = 1f;

        if(drawable == null) {
            return;
        }

        final float viewWidth = getWidth();
        final float viewHeight = getHeight();
        final int drawableWidth = drawable.getIntrinsicWidth();
        final int drawableHeight = drawable.getIntrinsicHeight();
        translateX = viewWidth / 2f;
        translateY = viewHeight / 2f;

        if(viewWidth < viewHeight) {
            borderLength = (int) (viewWidth - 2 * BORDER_DISTANCE);
        } else {
            borderLength = (int) (viewHeight - 2 * BORDER_DISTANCE);
        }

        if(drawableWidth <= drawableHeight) {
            screenScale = (float) SingleEyeFragment.width / drawableWidth;
        } else {
            screenScale = (float) SingleEyeFragment.height / drawableHeight;
        }

        Log.i("default", defaultMatrix.toString());
        defaultMatrix.setScale(screenScale, screenScale);
        //defaultMatrix.setScale(4f, 4f);
        Log.i("default", defaultMatrix.toString());

        defaultMatrix.postTranslate(translateX - SingleEyeFrameView.eyeX, translateY - SingleEyeFrameView.eyeY);
        Log.i("default", defaultMatrix.toString());

        defaultMatrix.postScale(4f, 4f, translateX, translateY);
        Log.i("default", defaultMatrix.toString());

        /*float screenScale = 1f;

        if(drawableWidth <= drawableHeight) {
            screenScale = (float) borderlength / drawableWidth;
        } else {
            screenScale = (float) borderlength / drawableHeight;
        }

        defaultMatrix.setScale(screenScale, screenScale);

        if(drawableWidth <= drawableHeight) {
            float heightOffset = (viewHeight - drawableHeight * screenScale) / 2.0f;
            if(viewWidth <= viewHeight) {
                defaultMatrix.postTranslate(BORDER_DISTANCE, heightOffset);
            } else {
                defaultMatrix.postTranslate((viewWidth - borderlength) / 2.0f, heightOffset);
            }
        } else {
            float widthOffset = (viewWidth - drawableWidth * screenScale) / 2.0f;
            if(viewWidth <= viewHeight) {
                defaultMatrix.postTranslate(widthOffset, (viewHeight - borderlength) / 2.0f);
            } else {
                defaultMatrix.postTranslate(widthOffset, BORDER_DISTANCE);
            }
        }*/

        resetMatrix();
    }

    /**
     * Resets the Matrix back to FIT_CENTER, and then displays it.s
     */
    private void resetMatrix() {
        if(dragMatrix == null) {
            return;
        }

        dragMatrix.reset();
        setImageMatrix(getDisplayMatrix());
    }

    private Matrix getDisplayMatrix() {
        Log.i("final", finalMatrix.toString());
        finalMatrix.set(defaultMatrix);
        Log.i("final", finalMatrix.toString());
        finalMatrix.postConcat(dragMatrix);
        Log.i("final", finalMatrix.toString());
        return finalMatrix;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return multiGestureDetector.onTouchEvent(motionEvent);
    }

    private class MultiGestureDetector extends GestureDetector.SimpleOnGestureListener implements
            ScaleGestureDetector.OnScaleGestureListener {

        private final ScaleGestureDetector scaleGestureDetector;
        private final GestureDetector gestureDetector;
        private final float scaledTouchSlop;

        private VelocityTracker velocityTracker;
        private boolean isDragging;

        private float lastTouchX;
        private float lastTouchY;
        private float lastPointerCount;

        public MultiGestureDetector(Context context) {
            scaleGestureDetector = new ScaleGestureDetector(context, this);
            gestureDetector = new GestureDetector(context, this);
            gestureDetector.setOnDoubleTapListener(this);

            final ViewConfiguration configuration = ViewConfiguration.get(context);
            scaledTouchSlop = configuration.getScaledTouchSlop();
        }

        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            float scale = getScale();
            float scaleFactor = scaleGestureDetector.getScaleFactor();
            if(getDrawable() != null && ((scale < maxScale && scaleFactor > 1.0f) || (scale > minScale && scaleFactor < 1.0f))){
                if(scaleFactor * scale < minScale){
                    scaleFactor = minScale / scale;
                }
                if(scaleFactor * scale > maxScale){
                    scaleFactor = maxScale / scale;
                }
                dragMatrix.postScale(scaleFactor, scaleFactor, getWidth() / 2, getHeight() / 2);
                checkAndDisplayMatrix();
            }
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {}

        public boolean onTouchEvent(MotionEvent event) {
            if (gestureDetector.onTouchEvent(event)) {
                return true;
            }

            scaleGestureDetector.onTouchEvent(event);

            /*
             * Get the center x, y of all the pointers
             */
            float x = 0, y = 0;
            final int pointerCount = event.getPointerCount();
            for (int i = 0; i < pointerCount; i++) {
                x += event.getX(i);
                y += event.getY(i);
            }
            x = x / pointerCount;
            y = y / pointerCount;

            /*
             * If the pointer count has changed cancel the drag
             */
            if (pointerCount != lastPointerCount) {
                isDragging = false;
                if (velocityTracker != null) {
                    velocityTracker.clear();
                }
                lastTouchX = x;
                lastTouchY = y;
                lastPointerCount = pointerCount;
            }

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (velocityTracker == null) {
                        velocityTracker = VelocityTracker.obtain();
                    } else {
                        velocityTracker.clear();
                    }
                    velocityTracker.addMovement(event);

                    lastTouchX = x;
                    lastTouchY = y;
                    isDragging = false;
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    lastPointerCount = 0;
                    if (velocityTracker != null) {
                        velocityTracker.recycle();
                        velocityTracker = null;
                    }
                    break;
                case MotionEvent.ACTION_MOVE: {
                    final float dx = x - lastTouchX, dy = y - lastTouchY;

                    if (isDragging == false) {
                        // Use Pythagoras to see if drag length is larger than
                        // touch slop
                        isDragging = Math.sqrt((dx * dx) + (dy * dy)) >= scaledTouchSlop;
                    }

                    if (isDragging) {
                        if (getDrawable() != null) {
                            dragMatrix.postTranslate(dx, dy);
                            checkAndDisplayMatrix();
                        }

                        lastTouchX = x;
                        lastTouchY = y;

                        if (velocityTracker != null) {
                            velocityTracker.addMovement(event);
                        }
                    }
                    break;
                }
            }

            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent event) {
            try {
                float scale = getScale();
                float x = getWidth() / 2;
                float y = getHeight() / 2;

                if (scale < midScale) {
                    post(new AnimatedZoomRunnable(scale, midScale, x, y));
                } else if ((scale >= midScale) && (scale < maxScale)) {
                    post(new AnimatedZoomRunnable(scale, maxScale, x, y));
                } else {
                    post(new AnimatedZoomRunnable(scale, minScale, x, y));
                }
            } catch (Exception e) {
                // Can sometimes happen when getX() and getY() is called
            }

            return true;
        }
    }

    private class AnimatedZoomRunnable implements Runnable {
        // These are 'postScale' values, means they're compounded each iteration
        static final float ANIMATION_SCALE_PER_ITERATION_IN = 1.07f;
        static final float ANIMATION_SCALE_PER_ITERATION_OUT = 0.93f;

        private final float focalX, focalY;
        private final float targetZoom;
        private final float deltaScale;

        public AnimatedZoomRunnable(final float currentZoom, final float targetZoom,
                                    final float focalX, final float focalY) {
            this.targetZoom = targetZoom;
            this.focalX = focalX;
            this.focalY = focalY;

            if (currentZoom < targetZoom) {
                deltaScale = ANIMATION_SCALE_PER_ITERATION_IN;
            } else {
                deltaScale = ANIMATION_SCALE_PER_ITERATION_OUT;
            }
        }

        @Override
        public void run() {
            dragMatrix.postScale(deltaScale, deltaScale, focalX, focalY);
            checkAndDisplayMatrix();

            final float currentScale = getScale();

            if (((deltaScale > 1f) && (currentScale < targetZoom))
                    || ((deltaScale < 1f) && (targetZoom < currentScale))) {
                // We haven't hit our target scale yet, so post ourselves
                // again
                postOnAnimation(SingleEyeCropView.this, this);

            } else {
                // We've scaled past our target zoom, so calculate the
                // necessary scale so we're back at target zoom
                final float delta = targetZoom / currentScale;
                dragMatrix.postScale(delta, delta, focalX, focalY);
                checkAndDisplayMatrix();
            }
        }
    }

    private void postOnAnimation(View view, Runnable runnable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.postOnAnimation(runnable);
        } else {
            view.postDelayed(runnable, 16);
        }
    }

    /**
     * Returns the current scale value
     *
     * @return float - current scale value
     */
    public final float getScale() {
        dragMatrix.getValues(matrixValues);
        return matrixValues[Matrix.MSCALE_X];
    }

    public final float getScale(Matrix matrix){
        matrix.getValues(matrixV);
        return matrixV[Matrix.MSCALE_X];
    }

    /**
     * Helper method that simply checks the Matrix, and then displays the result
     */
    private void checkAndDisplayMatrix() {
        checkMatrixBounds();
        setImageMatrix(getDisplayMatrix());
    }

    private void checkMatrixBounds() {
        final RectF rect = getDisplayRect(getDisplayMatrix());
        if (null == rect) {
            return;
        }

        float deltaX = 0, deltaY = 0;
        final float viewWidth = getWidth();
        final float viewHeight = getHeight();

        final float heightBorder = (viewHeight - borderLength) / 2;
        final float weightBorder = (viewWidth - borderLength) / 2;
        if(rect.top > heightBorder){
            deltaY = heightBorder - rect.top;
        }
        if(rect.bottom < (viewHeight - heightBorder)){
            deltaY = viewHeight - heightBorder - rect.bottom;
        }
        if(rect.left > weightBorder){
            deltaX = weightBorder - rect.left;
        }
        if(rect.right < viewWidth - weightBorder){
            deltaX = viewWidth - weightBorder - rect.right;
        }
        // Finally actually translate the matrix
        dragMatrix.postTranslate(deltaX, deltaY);


    }

    private RectF getDisplayRect(Matrix matrix) {
        Drawable d = getDrawable();
        if (null != d) {
            displayRect.set(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
            matrix.mapRect(displayRect);
            return displayRect;
        }

        return null;
    }

    public Bitmap crop(){
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        draw(canvas);
        defaultScale = getScale(defaultMatrix);
        finalScale = getScale(finalMatrix);
        distance = defaultScale * (borderLength) / finalScale / 4f;
        if(MainActivity.firstEye) {
            firstLeft = defaultScale * ((getWidth() - borderLength) / 2 - matrixV[2]) / finalScale / 4f;
            firstTop = defaultScale * (BORDER_DISTANCE - matrixV[5]) / finalScale / 4f;
            firstRight = firstLeft + distance;
            firstBottom = firstTop + distance;
        } else{
            secondLeft = defaultScale * ((getWidth() - borderLength) / 2 - matrixV[2]) / finalScale / 4f;
            secondTop = defaultScale * (BORDER_DISTANCE - matrixV[5]) / finalScale / 4f;
            secondRight = secondLeft + distance;
            secondBottom = secondTop + distance;
        }
        return Bitmap.createBitmap(bitmap, (getWidth() - borderLength) / 2, (getHeight() - borderLength) / 2, borderLength, borderLength);
    }
}
