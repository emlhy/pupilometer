package com.example.demonpilot.pupilometer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demonpilot.pupilometer.item.ImageItem;
import com.example.demonpilot.pupilometer.R;

import java.util.ArrayList;

/**
 * Created by Demon Pilot on 2015/10/5.
 */
public class GalleryAdapter extends BaseAdapter {
    private LayoutInflater _inflater;
    private ArrayList<ImageItem> _imageItem = new ArrayList<ImageItem>();

    public GalleryAdapter(Context context, ArrayList<ImageItem> imageItem) {
        super();
        _imageItem = imageItem;
        _inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return _imageItem.size();
    }

    @Override
    public Object getItem(int position) {
        return _imageItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //ImageView photoView;
        //TextView name;
        View view;
        //ViewHolder viewHolder;
        if(convertView == null) {
            view = _inflater.inflate(R.layout.item_gallery, null);
            //convertView = _inflater.inflate(R.layout.item_gallery, null);
            //photoView = (ImageView) convertView.findViewById(R.id.image);
            //name = (TextView) convertView.findViewById(R.id.name);
            //viewHolder = new ViewHolder();
            //convertView.setTag(viewHolder);
        } else{
            view = convertView;
            //viewHolder = (ViewHolder)convertView.getTag();
        }

        ImageView photoView = (ImageView) view.findViewById(R.id.image);
        TextView name = (TextView) view.findViewById(R.id.name);

        //viewHolder.photoView = (ImageView) convertView.findViewById(R.id.image);
        //viewHolder.name = (TextView) convertView.findViewById(R.id.name);
        if (photoView != null) {
            photoView.setImageBitmap(_imageItem.get(position).get_bitmap());
        }

        if (name != null) {
            String show = _imageItem.get(position).get_name().substring(0, _imageItem.get(position).get_name().lastIndexOf("."));
            name.setText(show);
        }


        return view;
    }

    static class ViewHolder {
        ImageView photoView;
        TextView name;
    }
}
