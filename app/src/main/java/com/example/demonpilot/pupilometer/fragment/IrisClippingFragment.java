package com.example.demonpilot.pupilometer.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.demonpilot.pupilometer.view.CropView;
import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Demon Pilot on 2015/9/16.
 */
public class IrisClippingFragment extends Fragment {
    private Button previous;
    private Button next;
    private TextView eyeText;
    private CropView cropView;
    private String path;
    private String tempPathLeft;
    private String tempPathRight;
    private Uri imageUri;
    private Bitmap bitmap;
    private Bitmap cropBitmap;
    private byte[] data;
    private File mediaStorageDir;

    public IrisClippingFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_iris_clipping, container, false);
        //((ActionBarActivity)getActivity()).getSupportActionBar().hide();
        path = getArguments().getString("path");
        if(getArguments().getString("temp_path_left") != null){
            tempPathLeft = getArguments().getString("temp_path_left");
        }
        /*try {
            imageUri = Uri.parse(path);
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        bitmap = BitmapFactory.decodeFile(path, options);

        previous = (Button)rootView.findViewById(R.id.previous);
        next = (Button)rootView.findViewById(R.id.next);
        eyeText = (TextView)rootView.findViewById(R.id.eye_text);
        cropView = (CropView)rootView.findViewById(R.id.crop_view);
        cropView.setImageBitmap(bitmap);
        if(MainActivity.leftEye) {
            eyeText.setText("Left eye");
        } else{
            eyeText.setText("Right eye");
        }
        MainActivity.dialog.dismiss();

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.leftEye = true;
                broadcastPopBackStack();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropBitmap = cropView.crop();
                //ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                //cropBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                //data = byteArrayOutputStream.toByteArray();
                if (MainActivity.leftEye) {
                    saveImage(cropBitmap);
                    MainActivity.leftEye = false;
                    broadcastReopen();
                } else {
                    saveImage(cropBitmap);
                    MainActivity.leftEye = true;
                    broadcastOpenPupilRatio();
                }
                //saveImage(cropBitmap);
                //broadcastOpenPupilRatio();
            }
        });

        //cropView.setImageBitmap(bitmap);

        return rootView;
    }

    private void saveImage(Bitmap bitmap){
        File imageFile = getOutputMediaFile();
        if (imageFile == null) {
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getOutputMediaFile(){

        mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PupiloMeter");

        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }

        File mediaFile;
        if(MainActivity.leftEye) {
            tempPathLeft = path.substring(0, path.lastIndexOf(".")) + "_Left.jpg";
            mediaFile = new File(tempPathLeft);
        } else{
            tempPathRight = path.substring(0, path.lastIndexOf(".")) + "_Right.jpg";
            mediaFile = new File(tempPathRight);
        }


        return mediaFile;
    }

    /*public Bitmap getThumbnail(Uri uri,int size) throws FileNotFoundException, IOException{
        InputStream input = getActivity().getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither=true;//optional
        onlyBoundsOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;
        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;
        double ratio = (originalSize > size) ? (originalSize / size) : 1.0;
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither=true;//optional
        bitmapOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
        input = getActivity().getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }
    private int getPowerOfTwoForSampleRatio(double ratio){
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if(k==0) return 1;
        else return k;
    }*/

    private void broadcastOpenPupilRatio() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_PUPIL_RATIO);
        intent.putExtra("temp_path_left", tempPathLeft);
        intent.putExtra("temp_path_right", tempPathRight);
        //intent.putExtra("path", path);
        //intent.putExtra("bitmap_array", data);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastReopen(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_REOPEN_IRIS_CLIPPING);
        intent.putExtra("temp_path_left", tempPathLeft);
        intent.putExtra("path", path);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastOpenCamera() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_SINGLE);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastPopBackStack() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_POP_BACK_STACK);
        getActivity().sendBroadcast(intent);
    }


}
