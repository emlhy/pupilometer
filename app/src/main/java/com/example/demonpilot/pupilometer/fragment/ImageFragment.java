package com.example.demonpilot.pupilometer.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by Demon Pilot on 2015/10/5.
 */
public class ImageFragment extends Fragment {
    private String mPath;
    private FileInputStream fis;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_image, container, false);
        //((ActionBarActivity)getActivity()).getSupportActionBar().hide();

        mPath = getArguments().getString("path");

        ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
        TextView textView = (TextView) rootView.findViewById(R.id.info);

        try {
            fis = new FileInputStream(mPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String info = mPath;
        info = mPath.substring(mPath.lastIndexOf("/") + 1, info.length());
        info = info.substring(0, info.lastIndexOf("."));
        Bitmap bitmap = BitmapFactory.decodeStream(fis);
        bitmap = Bitmap.createBitmap(bitmap,0, 0, bitmap.getWidth(), bitmap.getHeight());
        imageView.setImageBitmap(bitmap);
        textView.setText(info);
        textView.setTextSize(20);
        return rootView;
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            File file = new File(mPath);
            if(file.exists()) {
                if(file.delete()) {
                    Toast.makeText(getActivity().getApplicationContext(), "Photo deleted", Toast.LENGTH_SHORT).show();
                }
            }
            broadcastPopBackStack();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    private void broadcastPopBackStack() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_POP_BACK_STACK);
        getActivity().sendBroadcast(intent);
    }
}
