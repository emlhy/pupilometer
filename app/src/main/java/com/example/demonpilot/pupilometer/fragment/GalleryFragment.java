package com.example.demonpilot.pupilometer.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.Toast;

import com.example.demonpilot.pupilometer.adapter.GalleryAdapter;
import com.example.demonpilot.pupilometer.item.ImageItem;
import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;

/**
 * Created by Demon Pilot on 2015/10/5.
 */
public class GalleryFragment extends Fragment {
    private ArrayList<ImageItem> mImage = new ArrayList<ImageItem>();
    //private File file;
    private GalleryAdapter galleryAdapter;
    private Bitmap photo;
    private Bitmap square;
    private String name;
    private String path;
    private BitmapFactory.Options options;
    private File[] array;
    private GridView gridView;
    //private Map<String, Boolean> selectMap;
    //private int squareSize;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
        //((ActionBarActivity)getActivity()).getSupportActionBar().hide();

        gridView = (GridView) rootView.findViewById(R.id.gridView);
        //squareSize = (int) getResources().getDimension(R.dimen.square_size);
        //file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "PupilMeter");
        gridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        gridView.setMultiChoiceModeListener(new MultiChoiceModeListener());
        gridView.setDrawSelectorOnTop(true);
        gridView.setSelector(getResources().getDrawable(R.drawable.gridview_selector));

        //setHasOptionsMenu(true);
        MainActivity.dialog = new ProgressDialog(getActivity());
        MainActivity.dialog.setMessage("Loading...");
        MainActivity.dialog.setCancelable(false);
        MainActivity.dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        MainActivity.dialog.show();
        //selectMap = new HashMap<String, Boolean>();
        new LoadDataTask(getActivity()).execute();

        /*if (mImage.size() == 0) {
            Toast.makeText(getActivity().getApplicationContext(), "No photo exist", Toast.LENGTH_SHORT).show();
        } else {
            galleryAdapter = new GalleryAdapter(getActivity(), mImage);
            gridView.setAdapter(galleryAdapter);
        }*/

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mImage.get(position) instanceof ImageItem) {
                    ImageItem item = (ImageItem) mImage.get(position);
                    broadcastOpenImage(item.get_path());
                }
            }
        });

        /*gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(mImage.get(position) instanceof ImageItem) {
                    ImageItem item = (ImageItem) mImage.get(position);
                    File file = new File(item.get_path());
                    Uri imageUri = Uri.fromFile(file);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, imageUri);
                    intent.setType("image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(intent, "Send image with"));
                }
                return true;
            }
        });*/
        return rootView;
    }

    private class LoadDataTask extends AsyncTask<Void, Void, Integer> {
        private Context _context;

        public LoadDataTask(Context context) {
            _context = context;
        }

        @Override
        protected void onPreExecute() {
            photo = null;
            square = null;
            name = null;
            path = null;
            mImage.clear();
            options = new BitmapFactory.Options();
            array = MainActivity.mediaStorageDir.listFiles();
        }

        @Override
        protected Integer doInBackground(Void... arg0) {
            for (int i = 0; i < array.length; i++) {
                if (array[i].isFile()) {
                    options.inSampleSize = 8;
                    photo = BitmapFactory.decodeFile(array[i].getPath(), options);
                    path = array[i].getPath();
                    if (photo.getWidth() >= photo.getHeight()){
                        square = Bitmap.createBitmap(photo, photo.getWidth() / 2 - photo.getHeight() / 2, 0, photo.getHeight(), photo.getHeight());
                    }else{
                        square = Bitmap.createBitmap(photo, 0, photo.getHeight()/2 - photo.getWidth()/2, photo.getWidth(), photo.getWidth());
                    }
                    //bitmap = Bitmap.createScaledBitmap(square, squareSize, squareSize, true);
                    name = array[i].getName();
                    mImage.add(new ImageItem(square, name, path, false));
                }
            }
            sortImages(mImage);
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (mImage.size() == 0) {
                Toast.makeText(getActivity().getApplicationContext(), "No photo exist", Toast.LENGTH_SHORT).show();
            } else {
                galleryAdapter = new GalleryAdapter(getActivity(), mImage);
                gridView.setAdapter(galleryAdapter);
                galleryAdapter.notifyDataSetChanged();
            }
            MainActivity.dialog.dismiss();
        }
    }

    private void sortImages(ArrayList<ImageItem> images) {
        Collections.sort(images, new Comparator<ImageItem>() {
            @Override
            public int compare(ImageItem lhs, ImageItem rhs) {
                return lhs.get_name().toLowerCase(Locale.ENGLISH)
                        .compareTo(rhs.get_name().toLowerCase(Locale.ENGLISH));
            }
        });
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete) {
            return true;
        }
        if (id == R.id.action_send) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    public class MultiChoiceModeListener implements GridView.MultiChoiceModeListener{
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.setTitle("Select Images");
            mode.setSubtitle("1 image selected");
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_main, menu);
            menu.findItem(R.id.action_select_all).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            menu.findItem(R.id.action_select_none).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            menu.findItem(R.id.action_delete).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            menu.findItem(R.id.action_send).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            final ActionMode _mode = mode;
            switch(item.getItemId()){
                case R.id.action_select_all:
                    for (int i = 0; i < gridView.getCount(); i++) {
                        gridView.setItemChecked(i, true);
                        ImageItem imageItem = mImage.get(i);
                        imageItem.set_selected(true);
                        mImage.set(i, imageItem);
                    }
                    break;
                case R.id.action_select_none:
                    for (int i = 0; i < gridView.getCount(); i++) {
                        gridView.setItemChecked(i, false);
                        ImageItem imageItem = mImage.get(i);
                        imageItem.set_selected(false);
                        mImage.set(i, imageItem);
                    }
                    break;
                case R.id.action_delete:
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Delete images")
                            .setMessage("Do you want to delete the images?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    for(Iterator<ImageItem> iterator = mImage.iterator(); iterator.hasNext();){
                                        ImageItem imageItem = iterator.next();
                                        if(imageItem.get_selected()){
                                            File f = new File(imageItem.get_path());
                                            f.delete();
                                            iterator.remove();
                                        }
                                    }
                                    _mode.finish();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    _mode.finish();
                                }
                            }).show();

                    break;
                case R.id.action_send:
                    ArrayList<Uri> imageUris = new ArrayList<Uri>();
                    for(Iterator<ImageItem> iterator = mImage.iterator(); iterator.hasNext();){
                        ImageItem imageItem = iterator.next();
                        if(imageItem.get_selected()){
                            File f = new File(imageItem.get_path());
                            Uri imageUri = Uri.fromFile(f);
                            imageUris.add(imageUri);
                        }
                    }
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND_MULTIPLE);
                    intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
                    intent.setType("image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(intent, "Send image with"));
                    mode.finish();
                    break;
            }
            return true;
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            int selectCount = gridView.getCheckedItemCount();
            switch (selectCount) {
                case 1:
                    mode.setSubtitle("1 image selected");
                    break;
                default:
                    mode.setSubtitle("" + selectCount + " images selected");
                    break;
            }
            if(checked){
                ((FrameLayout)(gridView.getChildAt(position - gridView.getFirstVisiblePosition()))).setForeground(getResources().getDrawable(R.drawable.gridview_selector));
                ImageItem imageItem = mImage.get(position);
                imageItem.set_selected(true);
                mImage.set(position, imageItem);
            } else{
                ImageItem imageItem = mImage.get(position);
                imageItem.set_selected(false);
                mImage.set(position, imageItem);
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            galleryAdapter.notifyDataSetChanged();
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }
    }

    private void broadcastOpenImage(String path) {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_IMAGE);
        intent.putExtra("path", path);
        getActivity().sendBroadcast(intent);
    }
}
