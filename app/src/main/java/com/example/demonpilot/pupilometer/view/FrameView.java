package com.example.demonpilot.pupilometer.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.example.demonpilot.pupilometer.fragment.CameraFragment;

/**
 * Created by Demon Pilot on 2015/10/20.
 */
public class FrameView extends View {
    private Paint line;
    //private Path path;
    //private Path left;
    //private Path right;
    private Path topLeft;
    private Path topRight;
    private Path bottomLeft;
    private Path bottomRight;
    //private Path centerHorizontal;
    //private Path centerVertical;
    private DashPathEffect dashPath;

    public static float leftEyeCenterX;
    public static float leftEyeCenterY;
    public static float rightEyeCenterX;
    public static float rightEyeCenterY;

    public FrameView(Context context) {
        super(context);
    }

    public FrameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FrameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        dashPath = new DashPathEffect(new float[]{4, 4}, 1);
        float top = CameraFragment.position[1] - CameraFragment.topOffset - dpToPx(20);
        float bottom = CameraFragment.position[1] + CameraFragment.height - CameraFragment.topOffset - dpToPx(20);
        float l = CameraFragment.position[0] - dpToPx(16);
        float r = CameraFragment.position[0] + CameraFragment.width - dpToPx(16);
        float centerX = CameraFragment.width / 2;
        float centerY = top + CameraFragment.height / 2;
        float eyeWidth = (CameraFragment.width - dpToPx(40)) / 3;
        //path = new Path();
        //left = new Path();
        //right = new Path();
        topLeft = new Path();
        topRight = new Path();
        bottomLeft = new Path();
        bottomRight = new Path();
        //centerHorizontal = new Path();
        //centerVertical = new Path();
        //path.moveTo(centerX, top);
        //path.lineTo(centerX, bottom);
        /*left.moveTo(centerX - centerX / 4, top + dpToPx(20));
        left.lineTo(centerX + centerX / 4, top + dpToPx(20));
        right.moveTo(centerX - centerX / 4, bottom - dpToPx(20));
        right.lineTo(centerX + centerX / 4, bottom - dpToPx(20));
        topLeft.moveTo(centerX - centerX / 4 - dpToPx(10), top + dpToPx(30));
        topLeft.lineTo(centerX - centerX / 4 - dpToPx(10), top + CameraFragment.height / 3);
        topRight.moveTo(centerX + centerX / 4 + dpToPx(10), top + dpToPx(30));
        topRight.lineTo(centerX + centerX / 4 + dpToPx(10), top + CameraFragment.height / 3);
        bottomLeft.moveTo(centerX - centerX / 4 - dpToPx(10), bottom - dpToPx(30));
        bottomLeft.lineTo(centerX - centerX / 4 - dpToPx(10), bottom - CameraFragment.height / 3);
        bottomRight.moveTo(centerX + centerX / 4 + dpToPx(10), bottom - dpToPx(30));
        bottomRight.lineTo(centerX + centerX / 4 + dpToPx(10), bottom - CameraFragment.height / 3);
        centerHorizontal.moveTo(centerX, centerY - dpToPx(10));
        centerHorizontal.lineTo(centerX, centerY + dpToPx(10));
        centerVertical.moveTo(centerX - dpToPx(10), centerY);
        centerVertical.lineTo(centerX + dpToPx(10), centerY);
        left.moveTo(l + dpToPx(10), centerY - dpToPx(90));
        left.lineTo(l + dpToPx(10), centerY - dpToPx(10));
        right.moveTo(r - dpToPx(10), centerY - dpToPx(90));
        right.lineTo(r - dpToPx(10), centerY - dpToPx(10));
        topLeft.moveTo(l + dpToPx(20), centerY - dpToPx(100));
        topLeft.lineTo(l + CameraFragment.width / 4, centerY - dpToPx(100));
        topRight.moveTo(r - dpToPx(20), centerY - dpToPx(100));
        topRight.lineTo(r - CameraFragment.width / 4, centerY - dpToPx(100));
        bottomLeft.moveTo(l + dpToPx(20), centerY);
        bottomLeft.lineTo(l + CameraFragment.width / 4, centerY);
        bottomRight.moveTo(r - dpToPx(20), centerY);
        bottomRight.lineTo(r - CameraFragment.width / 4, centerY);
        centerHorizontal.moveTo(centerX - dpToPx(10), centerY - dpToPx(50));
        centerHorizontal.lineTo(centerX + dpToPx(10), centerY - dpToPx(50));
        centerVertical.moveTo(centerX, centerY - dpToPx(60));
        centerVertical.lineTo(centerX, centerY - dpToPx(40));*/
        leftEyeCenterX = l + dpToPx(20) + eyeWidth / 2;
        leftEyeCenterY = centerY - dpToPx(45);
        rightEyeCenterX = r - dpToPx(20) - eyeWidth / 2;
        rightEyeCenterY = centerY - dpToPx(45);
        topLeft.moveTo(l + dpToPx(20), centerY - dpToPx(50));
        topLeft.cubicTo(l + dpToPx(20) + eyeWidth / 3, centerY - dpToPx(80), l + dpToPx(20) + eyeWidth * 2 / 3, centerY - dpToPx(80), l + dpToPx(20) + eyeWidth, centerY - dpToPx(50));
        bottomLeft.moveTo(l + dpToPx(20), centerY - dpToPx(40));
        bottomLeft.cubicTo(l + dpToPx(20) + eyeWidth / 3, centerY - dpToPx(10), l + dpToPx(20) + eyeWidth * 2 / 3, centerY - dpToPx(10), l + dpToPx(20) + eyeWidth, centerY - dpToPx(40));
        topRight.moveTo(r - dpToPx(20), centerY - dpToPx(50));
        topRight.cubicTo(r - dpToPx(20) - eyeWidth / 3, centerY - dpToPx(80), r - dpToPx(20) - eyeWidth * 2 / 3, centerY - dpToPx(80), r - dpToPx(20) - eyeWidth, centerY - dpToPx(50));
        bottomRight.moveTo(r - dpToPx(20), centerY - dpToPx(40));
        bottomRight.cubicTo(r - dpToPx(20) - eyeWidth / 3, centerY - dpToPx(10), r - dpToPx(20) - eyeWidth * 2 / 3, centerY - dpToPx(10), r - dpToPx(20) - eyeWidth, centerY - dpToPx(40));
        line = new Paint();
        line.setAntiAlias(true);
        line.setARGB(255, 255, 0, 0);
        line.setPathEffect(dashPath);
        line.setStyle(Paint.Style.STROKE);
        line.setStrokeWidth(5.0f);
        //canvas.drawPath(path, line);
        //canvas.drawPath(left, line);
        //canvas.drawPath(right, line);
        canvas.drawPath(topLeft, line);
        canvas.drawPath(topRight, line);
        canvas.drawPath(bottomLeft, line);
        canvas.drawPath(bottomRight, line);
        //canvas.drawPoint(leftEyeCenterX, leftEyeCenterY, line);
        //canvas.drawPoint(rightEyeCenterX, rightEyeCenterY, line);
        //canvas.drawPath(centerHorizontal, line);
        //canvas.drawPath(centerVertical, line);
        //canvas.drawLine(CameraFragment.width / 2, CameraFragment.position[1] - CameraFragment.topOffset - (int)dpToPx(20), CameraFragment.width / 2, CameraFragment.position[1] + CameraFragment.height - CameraFragment.topOffset - (int)dpToPx(20), line);
    }

    public float dpToPx(float dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        float px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
}
