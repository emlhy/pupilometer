package com.example.demonpilot.pupilometer.item;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Demon Pilot on 2015/10/5.
 */
public class ImageItem{
    private Bitmap _bitmap;
    private String _name;
    private String _path;
    private Boolean _selected;

    public ImageItem(Bitmap bitmap, String name, String path, Boolean selected) {
        _bitmap = bitmap;
        _name = name;
        _path = path;
        _selected = selected;
    }

    public String get_path() {
        return _path;
    }

    public void set_path(String _path) {
        this._path = _path;
    }

    public Bitmap get_bitmap() {
        return _bitmap;
    }

    public void set_bitmap(Bitmap _bitmap) {
        this._bitmap = _bitmap;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public Boolean get_selected() {
        return _selected;
    }

    public void set_selected(Boolean _selected) {
        this._selected = _selected;
    }
}
