package com.example.demonpilot.pupilometer.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.demonpilot.pupilometer.view.CircleScopeView;
import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;
import com.example.demonpilot.pupilometer.view.SingleEyeCropView;

import java.io.File;
import java.text.DecimalFormat;

/**
 * Created by Demon Pilot on 2015/12/3.
 */
public class SingleEyeRatioFragment extends Fragment{
    private Button previous;
    private Button done;
    private SeekBar seekBarLeft;
    private SeekBar seekBarRight;
    private ImageView imageViewLeft;
    private ImageView imageViewRight;
    private CircleScopeView circleScopeView;
    private RelativeLayout relativeLayout;
    private TextView percentageLeft;
    private TextView percentageRight;
    private String firstPath;
    private String secondPath;
    private Bitmap leftEye;
    private Bitmap rightEye;
    private float currentDiameterLeft;
    private float currentDiameterRight;
    private float progressLeft;
    private float progressRight;
    public DecimalFormat decimalFormat;

    public SingleEyeRatioFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pupil_ratio, container, false);
        firstPath = getArguments().getString("first_path");
        secondPath = getArguments().getString("second_path");
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        leftEye = BitmapFactory.decodeFile(firstPath, options);
        rightEye = BitmapFactory.decodeFile(secondPath, options);

        relativeLayout = (RelativeLayout)rootView.findViewById(R.id.ratio_layout);

        circleScopeView = new CircleScopeView(getActivity());
        circleScopeView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        relativeLayout.addView(circleScopeView);

        previous = (Button)rootView.findViewById(R.id.previous);
        done = (Button)rootView.findViewById(R.id.done);
        seekBarLeft = (SeekBar)rootView.findViewById(R.id.seek_bar_left);
        percentageLeft = (TextView)rootView.findViewById(R.id.percentage_left);
        imageViewLeft = (ImageView)rootView.findViewById(R.id.iris_left);
        seekBarRight = (SeekBar)rootView.findViewById(R.id.seek_bar_right);
        percentageRight = (TextView)rootView.findViewById(R.id.percentage_right);
        imageViewRight = (ImageView)rootView.findViewById(R.id.iris_right);

        ViewGroup.LayoutParams layoutParamsLeft = imageViewLeft.getLayoutParams();
        layoutParamsLeft.width = (int)dpToPx(200);
        layoutParamsLeft.height = (int)dpToPx(200);
        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams)imageViewLeft.getLayoutParams();
        margin.topMargin = (int)dpToPx(20);
        imageViewLeft.setLayoutParams(layoutParamsLeft);
        imageViewLeft.setLayoutParams(margin);
        imageViewLeft.setImageBitmap(leftEye);
        ViewGroup.LayoutParams layoutParamsRight = imageViewRight.getLayoutParams();
        layoutParamsRight.width = (int)dpToPx(200);
        layoutParamsRight.height = (int)dpToPx(200);
        imageViewRight.setLayoutParams(layoutParamsRight);
        imageViewRight.setLayoutParams(margin);
        imageViewRight.setImageBitmap(rightEye);

        percentageLeft.setText("0.00mm(0%)");
        percentageRight.setText("0.00mm(0%)");
        decimalFormat = new DecimalFormat("0.00");

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.firstEye = false;
                broadcastPopBackStack();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File fromLeft = new File(firstPath);
                File toLeft = new File(firstPath.substring(0, firstPath.lastIndexOf("_")) + "_" + decimalFormat.format(currentDiameterLeft) + "mm(" + progressLeft + "%)_(" + decimalFormat.format(SingleEyeCropView.firstLeft) + "," + decimalFormat.format(SingleEyeCropView.firstTop) + ")_(" + decimalFormat.format(SingleEyeCropView.firstRight) + "," + decimalFormat.format(SingleEyeCropView.firstBottom) + ").jpg");
                fromLeft.renameTo(toLeft);
                File fromRight = new File(secondPath);
                File toRight = new File(secondPath.substring(0, secondPath.lastIndexOf("_")) + "_" + decimalFormat.format(currentDiameterRight) + "mm(" + progressRight + "%)_(" + decimalFormat.format(SingleEyeCropView.secondLeft) + "," + decimalFormat.format(SingleEyeCropView.secondTop) + ")_(" + decimalFormat.format(SingleEyeCropView.secondRight) + "," + decimalFormat.format(SingleEyeCropView.secondBottom) + ").jpg");
                fromRight.renameTo(toRight);
                MainActivity.firstEye = true;
                broadcastOpenMain();
            }
        });

        seekBarLeft.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                circleScopeView.setRadiusLeft(progress);
                circleScopeView.invalidate();
                progressLeft = progress;
                currentDiameterLeft = (float) (progress * 0.12);
                percentageLeft.setText(decimalFormat.format(currentDiameterLeft) + "mm(" + progress + "%)");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarRight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                circleScopeView.setRadiusRight(progress);
                circleScopeView.invalidate();
                progressRight = progress;
                currentDiameterRight = (float)(progress * 0.12);
                percentageRight.setText(decimalFormat.format(currentDiameterRight) + "mm(" + progress + "%)");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return rootView;
    }

    public float dpToPx(float dp) {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        float px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    private void broadcastPopBackStack() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_POP_BACK_STACK);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastOpenMain() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_MAIN);
        getActivity().sendBroadcast(intent);
    }
}
