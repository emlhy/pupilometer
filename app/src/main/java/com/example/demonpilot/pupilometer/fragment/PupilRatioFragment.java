package com.example.demonpilot.pupilometer.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.demonpilot.pupilometer.view.CircleScopeView;
import com.example.demonpilot.pupilometer.view.CropView;
import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;

import java.io.File;
import java.text.DecimalFormat;

/**
 * Created by Demon Pilot on 2015/9/16.
 */
public class PupilRatioFragment extends Fragment {
    private Button previous;
    private Button done;
    private SeekBar seekBarLeft;
    private SeekBar seekBarRight;
    private ImageView imageViewLeft;
    private ImageView imageViewRight;
    private float radius;
    private CircleScopeView circleScopeView;
    private RelativeLayout relativeLayout;
    private TextView percentageLeft;
    private TextView percentageRight;
    private byte[] data;
    private String tempPathLeft;
    private String tempPathRight;
    private Bitmap leftEye;
    private Bitmap rightEye;
    private float currentDiameterLeft;
    private float currentDiameterRight;
    private float progressLeft;
    private float progressRight;
    //private String path;
    public DecimalFormat decimalFormat;
    //public Bitmap mBitmap;

    public PupilRatioFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pupil_ratio, container, false);
        //((ActionBarActivity)getActivity()).getSupportActionBar().hide();
        //data = getArguments().getByteArray("bitmap_array");
        //bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        tempPathLeft = getArguments().getString("temp_path_left");
        tempPathRight = getArguments().getString("temp_path_right");
        //path = getArguments().getString("path");
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        leftEye = BitmapFactory.decodeFile(tempPathLeft, options);
        rightEye = BitmapFactory.decodeFile(tempPathRight, options);

        relativeLayout = (RelativeLayout)rootView.findViewById(R.id.ratio_layout);

        circleScopeView = new CircleScopeView(getActivity());
        circleScopeView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        relativeLayout.addView(circleScopeView);

        previous = (Button)rootView.findViewById(R.id.previous);
        done = (Button)rootView.findViewById(R.id.done);
        seekBarLeft = (SeekBar)rootView.findViewById(R.id.seek_bar_left);
        percentageLeft = (TextView)rootView.findViewById(R.id.percentage_left);
        imageViewLeft = (ImageView)rootView.findViewById(R.id.iris_left);
        seekBarRight = (SeekBar)rootView.findViewById(R.id.seek_bar_right);
        percentageRight = (TextView)rootView.findViewById(R.id.percentage_right);
        imageViewRight = (ImageView)rootView.findViewById(R.id.iris_right);

        /*Paint circle = new Paint();
        DashPathEffect dashPath = new DashPathEffect(new float[]{4, 4}, 1);
        circle.setAntiAlias(true);
        circle.setARGB(255, 255, 0, 0);
        circle.setPathEffect(dashPath);
        circle.setStyle(Paint.Style.STROKE);
        circle.setStrokeWidth(3.0f);
        mBitmap = Bitmap.createBitmap((int)dpToPx(200), (int)dpToPx(200), Bitmap.Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(mBitmap);
        tempCanvas.drawBitmap(bitmap, 0, 0, null);
        tempCanvas.drawCircle(dpToPx(100), dpToPx(100), dpToPx(100), circle);
        tempCanvas.drawCircle(dpToPx(100), dpToPx(100), dpToPx(getRadius()), circle);*/

        ViewGroup.LayoutParams layoutParamsLeft = imageViewLeft.getLayoutParams();
        layoutParamsLeft.width = (int)dpToPx(200);
        layoutParamsLeft.height = (int)dpToPx(200);
        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams)imageViewLeft.getLayoutParams();
        margin.topMargin = (int)dpToPx(20);
        imageViewLeft.setLayoutParams(layoutParamsLeft);
        imageViewLeft.setLayoutParams(margin);
        //imageView.setImageDrawable(new BitmapDrawable(getResources(), mBitmap));
        imageViewLeft.setImageBitmap(leftEye);
        ViewGroup.LayoutParams layoutParamsRight = imageViewRight.getLayoutParams();
        layoutParamsRight.width = (int)dpToPx(200);
        layoutParamsRight.height = (int)dpToPx(200);
        imageViewRight.setLayoutParams(layoutParamsRight);
        imageViewRight.setLayoutParams(margin);
        imageViewRight.setImageBitmap(rightEye);

        percentageLeft.setText("0.00mm(0%)");
        percentageRight.setText("0.00mm(0%)");
        decimalFormat = new DecimalFormat("0.00");

        /*Log.i("leftLeft", String.valueOf(CropView.leftLeft));
        Log.i("leftTop", String.valueOf(CropView.leftTop));
        Log.i("leftRight", String.valueOf(CropView.leftRight));
        Log.i("leftBottom", String.valueOf(CropView.leftBottom));
        Log.i("rightLeft", String.valueOf(CropView.rightLeft));
        Log.i("rightTop", String.valueOf(CropView.rightTop));
        Log.i("rightRight", String.valueOf(CropView.rightRight));
        Log.i("rightBottom", String.valueOf(CropView.rightBottom));

        final EditText input = new EditText(getActivity());
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Save file");
        alertDialog.setMessage("Input file name:");
        alertDialog.setView(input);
        alertDialog.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                File from = new File(MainActivity.mediaStorageDir.getPath(), "temp.jpg");
                File to = new File(MainActivity.mediaStorageDir.getPath(), input.getText() + ".jpg");
                from.renameTo(to);
                broadcastOpenMain();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });*/

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.leftEye = false;
                broadcastPopBackStack();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File fromLeft = new File(tempPathLeft);
                File toLeft = new File(tempPathLeft.substring(0, tempPathLeft.lastIndexOf(".")) + "_" + decimalFormat.format(currentDiameterLeft) + "mm(" + progressLeft + "%)_(" + decimalFormat.format(CropView.leftLeft) + "," + decimalFormat.format(CropView.leftTop) + ")_(" + decimalFormat.format(CropView.leftRight) + "," + decimalFormat.format(CropView.leftBottom) + ").jpg");
                fromLeft.renameTo(toLeft);
                File fromRight = new File(tempPathRight);
                File toRight = new File(tempPathRight.substring(0, tempPathRight.lastIndexOf(".")) + "_" + decimalFormat.format(currentDiameterRight) + "mm(" + progressRight + "%)_(" + decimalFormat.format(CropView.rightLeft) + "," + decimalFormat.format(CropView.rightTop) + ")_(" + decimalFormat.format(CropView.rightRight) + "," + decimalFormat.format(CropView.rightBottom) + ").jpg");
                fromRight.renameTo(toRight);
                MainActivity.leftEye = true;
                broadcastOpenMain();

                /*if(MainActivity.leftEye){
                    MainActivity.leftEye = false;
                    broadcastOpenIrisClipping();
                } else {
                    MainActivity.leftEye = true;
                    broadcastOpenMain();
                }*/
                /*alertDialog.show();
                String text = String.valueOf(currentProgress) + "%";
                input.setText(text);
                input.setSelection(text.length());*/
            }
        });

        seekBarLeft.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //circleScopeView.setRadius((float)(Math.sqrt(progress) * 100 / Math.sqrt(100)));
                circleScopeView.setRadiusLeft(progress);
                circleScopeView.invalidate();
                //setRadius(progress);
                //imageView.postInvalidate();
                /*int[] location = new int[2];
                imageViewLeft.getLocationInWindow(location);
                float y = px2dip(location[1]);
                imageViewRight.getLocationInWindow(location);
                y = px2dip(location[1]);
                float leftY = imageViewLeft.getY() + imageViewLeft.getHeight() / 2;
                float rightY = imageViewRight.getY() + imageViewRight.getHeight() / 2;*/
                progressLeft = progress;
                currentDiameterLeft = (float) (progress * 0.12);
                percentageLeft.setText(decimalFormat.format(currentDiameterLeft) + "mm(" + progress + "%)");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarRight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //circleScopeView.setRadius((float)(Math.sqrt(progress) * 100 / Math.sqrt(100)));
                circleScopeView.setRadiusRight(progress);
                circleScopeView.invalidate();
                //setRadius(progress);
                //imageView.postInvalidate();
                /*int[] location = new int[2];
                imageViewLeft.getLocationInWindow(location);
                float y = px2dip(location[1]);
                imageViewRight.getLocationInWindow(location);
                y = px2dip(location[1]);
                float leftY = imageViewLeft.getY() + imageViewLeft.getHeight() / 2;
                float rightY = imageViewRight.getY() + imageViewRight.getHeight() / 2;*/
                progressRight = progress;
                currentDiameterRight = (float)(progress * 0.12);
                percentageRight.setText(decimalFormat.format(currentDiameterRight) + "mm(" + progress + "%)");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return rootView;
    }

    /*public void setRadius(float radius) {
        this.radius = radius;
    }
    public float getRadius() {
        return radius;
    }*/

    /*public float px2dip(float pxValue) {
        final float scale = getActivity().getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }*/

    public float dpToPx(float dp) {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        float px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    private void broadcastPopBackStack() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_POP_BACK_STACK);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastOpenMain() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_MAIN);
        getActivity().sendBroadcast(intent);
    }
}
