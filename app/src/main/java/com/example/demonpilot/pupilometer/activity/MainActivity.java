package com.example.demonpilot.pupilometer.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.example.demonpilot.pupilometer.R;
import com.example.demonpilot.pupilometer.fragment.CameraFragment;
import com.example.demonpilot.pupilometer.fragment.GalleryFragment;
import com.example.demonpilot.pupilometer.fragment.ImageFragment;
import com.example.demonpilot.pupilometer.fragment.IrisClippingFragment;
import com.example.demonpilot.pupilometer.fragment.MainActivityFragment;
import com.example.demonpilot.pupilometer.fragment.PupilRatioFragment;
import com.example.demonpilot.pupilometer.fragment.Q10Fragment;
import com.example.demonpilot.pupilometer.fragment.Q11Fragment;
import com.example.demonpilot.pupilometer.fragment.Q1Fragment;
import com.example.demonpilot.pupilometer.fragment.Q2Fragment;
import com.example.demonpilot.pupilometer.fragment.Q3Fragment;
import com.example.demonpilot.pupilometer.fragment.Q4Fragment;
import com.example.demonpilot.pupilometer.fragment.Q5Fragment;
import com.example.demonpilot.pupilometer.fragment.Q6Fragment;
import com.example.demonpilot.pupilometer.fragment.Q7Fragment;
import com.example.demonpilot.pupilometer.fragment.Q8Fragment;
import com.example.demonpilot.pupilometer.fragment.Q9Fragment;
import com.example.demonpilot.pupilometer.fragment.ScoreFragment;
import com.example.demonpilot.pupilometer.fragment.SingleEyeClippingFragment;
import com.example.demonpilot.pupilometer.fragment.SingleEyeFragment;
import com.example.demonpilot.pupilometer.fragment.SingleEyeRatioFragment;

import java.io.File;


public class MainActivity extends ActionBarActivity {

    public static final String INTENT_OPEN_IRIS_CLIPPING = "INTENT_OPEN_IRIS_CLIPPING";
    public static final String INTENT_OPEN_PUPIL_RATIO = "INTENT_OPEN_PUPIL_RATIO";
    public static final String INTENT_POP_BACK_STACK = "INTENT_POP_BACK_STACK";
    public static final String INTENT_OPEN_MAIN = "INTENT_OPEN_MAIN";
    public static final String INTENT_OPEN_IMAGE = "INTENT_OPEN_IMAGE";
    public static final String INTENT_OPEN_GALLERY = "INTENT_OPEN_GALLERY";
    public static final String INTENT_OPEN_SINGLE = "INTENT_OPEN_SINGLE";
    public static final String INTENT_OPEN_DUAL = "INTENT_OPEN_DUAL";
    public static final String INTENT_REOPEN_IRIS_CLIPPING = "INTENT_REOPEN_IRIS_CLIPPING";
    public static final String INTENT_OPEN_SINGLE_EYE_CLIPPING = "INTENT_OPEN_SINGLE_EYE_CLIPPING";
    public static final String INTENT_REOPEN_SINGLE_EYE = "INTENT_REOPEN_SINGLE_EYE";
    public static final String INTENT_OPEN_SINGLE_EYE_RATIO = "INTENT_OPEN_SINGLE_EYE_RATIO";
    public static final String INTENT_OPEN_Q1 = "INTENT_OPEN_Q1";
    public static final String INTENT_OPEN_Q2 = "INTENT_OPEN_Q2";
    public static final String INTENT_OPEN_Q3 = "INTENT_OPEN_Q3";
    public static final String INTENT_OPEN_Q4 = "INTENT_OPEN_Q4";
    public static final String INTENT_OPEN_Q5 = "INTENT_OPEN_Q5";
    public static final String INTENT_OPEN_Q6 = "INTENT_OPEN_Q6";
    public static final String INTENT_OPEN_Q7 = "INTENT_OPEN_Q7";
    public static final String INTENT_OPEN_Q8 = "INTENT_OPEN_Q8";
    public static final String INTENT_OPEN_Q9 = "INTENT_OPEN_Q9";
    public static final String INTENT_OPEN_Q10 = "INTENT_OPEN_Q10";
    public static final String INTENT_OPEN_Q11 = "INTENT_OPEN_Q11";
    public static final String INTENT_OPEN_SCORE = "INTENT_OPEN_SCORE";

    private IrisClippingBroadcastReceiver _irisClippingBroadcastReceiver;
    private PupilRatioBroadcastReceiver _pupilRatioBroadcastReceiver;
    private PopBackStackBroadcastReceiver _popBackStackBroadcastReceiver;
    private MainBroadcastReceiver _mainBroadcastReceiver;
    private ImageBroadcastReceiver _imageBroadcastReceiver;
    private GalleryBroadcastReceiver _galleryBroadcastReceiver;
    private DualBroadcastReceiver _dualBroadcastReceiver;
    private SingleBroadcastReceiver _singleBroadcastReceiver;
    private ReopenBroadcastReceiver _reopenBroadcastReceiver;
    private SingleEyeClippingBroadcastReceiver _singleEyeClippingBroadcastReceiver;
    private ReopenSingleEyeBroadcastReceiver _reopenSingleEyeBroadcastReceiver;
    private SingleEyeRatioBroadcastReceiver _singleEyeRatioBroadcastReceiver;
    private Q1BroadcastReceiver _q1BroadcastReceiver;
    private Q2BroadcastReceiver _q2BroadcastReceiver;
    private Q3BroadcastReceiver _q3BroadcastReceiver;
    private Q4BroadcastReceiver _q4BroadcastReceiver;
    private Q5BroadcastReceiver _q5BroadcastReceiver;
    private Q6BroadcastReceiver _q6BroadcastReceiver;
    private Q7BroadcastReceiver _q7BroadcastReceiver;
    private Q8BroadcastReceiver _q8BroadcastReceiver;
    private Q9BroadcastReceiver _q9BroadcastReceiver;
    private Q10BroadcastReceiver _q10BroadcastReceiver;
    private Q11BroadcastReceiver _q11BroadcastReceiver;
    private ScoreBroadcastReceiver _scoreBroadcastReceiver;

    public static ProgressDialog dialog;
    public static File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "PupiloMeter");
    private String path;
    public static boolean leftEye;
    public static boolean firstEye;
    public static int totalScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //((ActionBarActivity)this).getSupportActionBar().hide();

        _irisClippingBroadcastReceiver = new IrisClippingBroadcastReceiver();
        _pupilRatioBroadcastReceiver = new PupilRatioBroadcastReceiver();
        _popBackStackBroadcastReceiver = new PopBackStackBroadcastReceiver();
        _mainBroadcastReceiver = new MainBroadcastReceiver();
        _imageBroadcastReceiver = new ImageBroadcastReceiver();
        _galleryBroadcastReceiver = new GalleryBroadcastReceiver();
        _dualBroadcastReceiver = new DualBroadcastReceiver();
        _singleBroadcastReceiver = new SingleBroadcastReceiver();
        _reopenBroadcastReceiver = new ReopenBroadcastReceiver();
        _singleEyeClippingBroadcastReceiver = new SingleEyeClippingBroadcastReceiver();
        _reopenSingleEyeBroadcastReceiver = new ReopenSingleEyeBroadcastReceiver();
        _singleEyeRatioBroadcastReceiver = new SingleEyeRatioBroadcastReceiver();
        _q1BroadcastReceiver = new Q1BroadcastReceiver();
        _q2BroadcastReceiver = new Q2BroadcastReceiver();
        _q3BroadcastReceiver = new Q3BroadcastReceiver();
        _q4BroadcastReceiver = new Q4BroadcastReceiver();
        _q5BroadcastReceiver = new Q5BroadcastReceiver();
        _q6BroadcastReceiver = new Q6BroadcastReceiver();
        _q7BroadcastReceiver = new Q7BroadcastReceiver();
        _q8BroadcastReceiver = new Q8BroadcastReceiver();
        _q9BroadcastReceiver = new Q9BroadcastReceiver();
        _q10BroadcastReceiver = new Q10BroadcastReceiver();
        _q11BroadcastReceiver = new Q11BroadcastReceiver();
        _scoreBroadcastReceiver = new ScoreBroadcastReceiver();
        leftEye = true;
        firstEye = true;
        totalScore = 0;

        if (findViewById(R.id.fragment) != null) {
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction().add(R.id.fragment, new MainActivityFragment()).commit();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(_irisClippingBroadcastReceiver);
        unregisterReceiver(_pupilRatioBroadcastReceiver);
        unregisterReceiver(_popBackStackBroadcastReceiver);
        unregisterReceiver(_mainBroadcastReceiver);
        unregisterReceiver(_imageBroadcastReceiver);
        unregisterReceiver(_galleryBroadcastReceiver);
        unregisterReceiver(_dualBroadcastReceiver);
        unregisterReceiver(_singleBroadcastReceiver);
        unregisterReceiver(_reopenBroadcastReceiver);
        unregisterReceiver(_singleEyeClippingBroadcastReceiver);
        unregisterReceiver(_reopenSingleEyeBroadcastReceiver);
        unregisterReceiver(_singleEyeRatioBroadcastReceiver);
        unregisterReceiver(_q1BroadcastReceiver);
        unregisterReceiver(_q2BroadcastReceiver);
        unregisterReceiver(_q3BroadcastReceiver);
        unregisterReceiver(_q4BroadcastReceiver);
        unregisterReceiver(_q5BroadcastReceiver);
        unregisterReceiver(_q6BroadcastReceiver);
        unregisterReceiver(_q7BroadcastReceiver);
        unregisterReceiver(_q8BroadcastReceiver);
        unregisterReceiver(_q9BroadcastReceiver);
        unregisterReceiver(_q10BroadcastReceiver);
        unregisterReceiver(_q11BroadcastReceiver);
        unregisterReceiver(_scoreBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(_irisClippingBroadcastReceiver, new IntentFilter(INTENT_OPEN_IRIS_CLIPPING));
        registerReceiver(_pupilRatioBroadcastReceiver, new IntentFilter(INTENT_OPEN_PUPIL_RATIO));
        registerReceiver(_popBackStackBroadcastReceiver, new IntentFilter(INTENT_POP_BACK_STACK));
        registerReceiver(_mainBroadcastReceiver, new IntentFilter(INTENT_OPEN_MAIN));
        registerReceiver(_imageBroadcastReceiver, new IntentFilter(INTENT_OPEN_IMAGE));
        registerReceiver(_galleryBroadcastReceiver, new IntentFilter(INTENT_OPEN_GALLERY));
        registerReceiver(_dualBroadcastReceiver, new IntentFilter(INTENT_OPEN_DUAL));
        registerReceiver(_singleBroadcastReceiver, new IntentFilter(INTENT_OPEN_SINGLE));
        registerReceiver(_reopenBroadcastReceiver, new IntentFilter(INTENT_REOPEN_IRIS_CLIPPING));
        registerReceiver(_singleEyeClippingBroadcastReceiver, new IntentFilter(INTENT_OPEN_SINGLE_EYE_CLIPPING));
        registerReceiver(_reopenSingleEyeBroadcastReceiver, new IntentFilter(INTENT_REOPEN_SINGLE_EYE));
        registerReceiver(_singleEyeRatioBroadcastReceiver, new IntentFilter(INTENT_OPEN_SINGLE_EYE_RATIO));
        registerReceiver(_q1BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q1));
        registerReceiver(_q2BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q2));
        registerReceiver(_q3BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q3));
        registerReceiver(_q4BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q4));
        registerReceiver(_q5BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q5));
        registerReceiver(_q6BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q6));
        registerReceiver(_q7BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q7));
        registerReceiver(_q8BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q8));
        registerReceiver(_q9BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q9));
        registerReceiver(_q10BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q10));
        registerReceiver(_q11BroadcastReceiver, new IntentFilter(INTENT_OPEN_Q11));
        registerReceiver(_scoreBroadcastReceiver, new IntentFilter(INTENT_OPEN_SCORE));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK){
            //if(data != null) {
                //Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                //saveImage(bitmap);
                path = MainActivityFragment.file.getAbsolutePath();
                openIrisClipping(path);
            //}
        }
    }*/

    /*private File getOutputMediaFile(){
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("Camera Guide", "Required media storage does not exist");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        path = mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg";
        mediaFile = new File(path);

        return mediaFile;
    }

    private void saveImage(Bitmap bitmap){
        File pictureFile = getOutputMediaFile();
        if(pictureFile == null) {
            return;
        }
        try{
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] data = byteArrayOutputStream.toByteArray();
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    public class IrisClippingBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String path = intent.getStringExtra("path");
            if(path != null) {
                openIrisClipping(path);
            }
        }
    }

    public class PupilRatioBroadcastReceiver extends BroadcastReceiver {
        @Override
         public void onReceive(Context context, Intent intent) {
            //byte[] data = intent.getByteArrayExtra("bitmap_array");
            String tempPathLeft = intent.getStringExtra("temp_path_left");
            String tempPathRight = intent.getStringExtra("temp_path_right");
            //String path = intent.getStringExtra("path");
            if(tempPathLeft != null && tempPathRight != null){
                openPupilRatio(tempPathLeft, tempPathRight);
            }
        }
    }

    public class PopBackStackBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            getSupportFragmentManager().popBackStack();
        }
    }

    public class DualBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            openDual();
        }
    }

    public class SingleBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            openSingle();
        }
    }

    public class GalleryBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openGallery();
        }
    }

    public class MainBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            openMain();
        }
    }

    public class ImageBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String path = intent.getStringExtra("path");
            if (path != null) {
                openImageFragment(path);
            }
        }
    }

    public class ReopenBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String tempPathLeft = intent.getStringExtra("temp_path_left");
            String path = intent.getStringExtra("path");
            if(tempPathLeft != null && path != null){
                reOpen(tempPathLeft, path);
            }
        }
    }

    public class SingleEyeClippingBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String firstPath = intent.getStringExtra("first_path");
            String secondPath = intent.getStringExtra("second_path");
            if(firstPath != null && secondPath != null){
                openSingleEyeClipping(firstPath, secondPath);
            }
        }
    }

    public class ReopenSingleEyeBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String firstPath = intent.getStringExtra("first_path");
            String secondPath = intent.getStringExtra("second_path");
            if(firstPath != null && secondPath != null){
                reOpenSingleEye(firstPath, secondPath);
            }
        }
    }

    public class SingleEyeRatioBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String firstPath = intent.getStringExtra("first_path");
            String secondPath = intent.getStringExtra("second_path");
            if(firstPath != null && secondPath != null){
                openSingleEyeRatio(firstPath, secondPath);
            }
        }
    }

    public class Q1BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ1();
        }
    }
    public class Q2BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ2();
        }
    }
    public class Q3BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ3();
        }
    }
    public class Q4BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ4();
        }
    }
    public class Q5BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ5();
        }
    }
    public class Q6BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ6();
        }
    }
    public class Q7BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ7();
        }
    }
    public class Q8BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ8();
        }
    }
    public class Q9BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ9();
        }
    }
    public class Q10BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ10();
        }
    }
    public class Q11BroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openQ11();
        }
    }
    public class ScoreBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            openScore();
        }
    }

    private void openIrisClipping(String path) {
        IrisClippingFragment frag = new IrisClippingFragment();
        Bundle bundle = new Bundle();
        bundle.putString("path", path);
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openSingleEyeClipping(String firstPath, String secondPath) {
        SingleEyeClippingFragment frag = new SingleEyeClippingFragment();
        Bundle bundle = new Bundle();
        bundle.putString("first_path", firstPath);
        bundle.putString("second_path", secondPath);
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openPupilRatio(String tempPathLeft, String tempPathRight) {
        PupilRatioFragment frag = new PupilRatioFragment();
        Bundle bundle = new Bundle();
        //bundle.putByteArray("bitmap_array", data);
        bundle.putString("temp_path_left", tempPathLeft);
        bundle.putString("temp_path_right", tempPathRight);
        //bundle.putString("path", path);
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void reOpen(String tempPathLeft, String path){
        IrisClippingFragment frag = new IrisClippingFragment();
        Bundle bundle = new Bundle();
        bundle.putString("temp_path_left", tempPathLeft);
        bundle.putString("path", path);
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void reOpenSingleEye(String firstPath, String secondPath){
        SingleEyeClippingFragment frag = new SingleEyeClippingFragment();
        Bundle bundle = new Bundle();
        bundle.putString("first_path", firstPath);
        bundle.putString("second_path", secondPath);
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openSingleEyeRatio(String firstPath, String secondPath){
        SingleEyeRatioFragment frag = new SingleEyeRatioFragment();Bundle bundle = new Bundle();
        bundle.putString("first_path", firstPath);
        bundle.putString("second_path", secondPath);
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openDual() {
        CameraFragment frag = new CameraFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openSingle() {
        SingleEyeFragment frag = new SingleEyeFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openGallery() {
        GalleryFragment frag = new GalleryFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openMain() {
        MainActivityFragment frag = new MainActivityFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openImageFragment(String path) {
        Bundle bundle = new Bundle();
        bundle.putString("path", path);
        ImageFragment frag = new ImageFragment();
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openQ1(){
        Q1Fragment frag = new Q1Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ2(){
        Q2Fragment frag = new Q2Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ3(){
        Q3Fragment frag = new Q3Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ4(){
        Q4Fragment frag = new Q4Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ5(){
        Q5Fragment frag = new Q5Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ6(){
        Q6Fragment frag = new Q6Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ7(){
        Q7Fragment frag = new Q7Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ8(){
        Q8Fragment frag = new Q8Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ9(){
        Q9Fragment frag = new Q9Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ10(){
        Q10Fragment frag = new Q10Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openQ11(){
        Q11Fragment frag = new Q11Fragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void openScore(){
        ScoreFragment frag = new ScoreFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = this.getSupportFragmentManager().findFragmentById(R.id.fragment);
        //FragmentManager fm = getSupportFragmentManager();
        //MainActivityFragment fragment = (MainActivityFragment)fm.findFragmentById(R.id.fragment);
        if(fragment instanceof MainActivityFragment){
            finish();
        } else if(fragment instanceof PupilRatioFragment) {
            leftEye = false;
            super.onBackPressed();
        } else if(fragment instanceof IrisClippingFragment) {
            leftEye = true;
            super.onBackPressed();
        } else if(fragment instanceof SingleEyeRatioFragment){
            firstEye = false;
            super.onBackPressed();
        } else if(fragment instanceof SingleEyeClippingFragment){
            firstEye = true;
            super.onBackPressed();
        } else{
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_gallery) {
            if(mediaStorageDir.exists() && mediaStorageDir.list().length != 0) {
                openGalleryFragment();
            } else{
                Toast.makeText(this.getApplicationContext(), "No image exist.", Toast.LENGTH_SHORT).show();
                return false;
            }
            return true;
        }
        if (id == R.id.action_camera) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, 1);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/


}
