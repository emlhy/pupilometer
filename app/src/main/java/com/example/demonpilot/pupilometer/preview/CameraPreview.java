package com.example.demonpilot.pupilometer.preview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.util.Log;
import android.view.TextureView;
import android.view.View;

import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.fragment.CameraFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Demon Pilot on 2015/9/27.
 */
public class CameraPreview extends TextureView implements TextureView.SurfaceTextureListener{

    private static Camera camera;
    public static String path;
    public static boolean isFlashOn;
    public static int cameraId;

    public CameraPreview(Context context) {
        super(context);
        //this.camera = camera;
        cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        final SurfaceTexture surfaceTexture = surface;
        if(camera == null){
            camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        }
        try {
            final Camera.Parameters parameters = camera.getParameters();
            List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
            /*for (int i = 0; i < previewSizes.size(); i++) {
                Camera.Size previewSize = previewSizes.get(i);
                Log.d("width: ", Integer.toString(previewSize.width));
                Log.d("height: ", Integer.toString(previewSize.height));
            }*/
            Camera.Size previewSize = previewSizes.get(1);
            parameters.setPreviewSize(previewSize.width, previewSize.height);
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

            isFlashOn = false;
            CameraFragment.flash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Camera.Parameters parameter = camera.getParameters();
                    if (isFlashOn) {
                        parameter.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        camera.setParameters(parameter);
                        CameraFragment.flash.setText("OFF");
                        isFlashOn = false;
                    } else {
                        parameter.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        camera.setParameters(parameter);
                        CameraFragment.flash.setText("ON");
                        isFlashOn = true;
                    }
                }
            });
            CameraFragment.switchCamera.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    camera.stopPreview();
                    camera.release();
                    if(cameraId == Camera.CameraInfo.CAMERA_FACING_BACK){
                        cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                        CameraFragment.switchCamera.setText("FRONT");
                        CameraFragment.flash.setText("OFF");
                        isFlashOn = false;
                        CameraFragment.flash.setVisibility(View.GONE);
                    } else {
                        cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                        CameraFragment.switchCamera.setText("BACK");
                        CameraFragment.flash.setVisibility(View.VISIBLE);
                    }
                    camera = Camera.open(cameraId);
                    camera.setDisplayOrientation(90);
                    try {
                        camera.setPreviewTexture(surfaceTexture);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    camera.startPreview();
                    camera.cancelAutoFocus();
                }
            });
            camera.setDisplayOrientation(90);
            camera.setParameters(parameters);
            camera.setPreviewTexture(surface);
            camera.startPreview();
            camera.cancelAutoFocus();
        } catch (IOException ioException) {

        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        camera.autoFocus(new Camera.AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean success, Camera camera) {
                if(success){
                    camera.cancelAutoFocus();
                }
            }

        });
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        camera.stopPreview();;
        camera.release();
        camera = null;
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    private File getOutputMediaFile(){
        if (! MainActivity.mediaStorageDir.exists()){
            if (! MainActivity.mediaStorageDir.mkdirs()){
                Log.d("Camera Guide", "Required media storage does not exist");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        File mediaFile;
        if(CameraPreview.isFlashOn){
            path = MainActivity.mediaStorageDir.getPath() + File.separator + timeStamp + "_Flash.jpg";
        } else{
            path = MainActivity.mediaStorageDir.getPath() + File.separator + timeStamp + ".jpg";
        }

        mediaFile = new File(path);

        return mediaFile;
    }

    /*public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        } catch (Exception e) {

        }
        return c;
    }*/

    private Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile();
            if(pictureFile == null) {
                return;
            }
            try{
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                Matrix matrix = new Matrix();
                if (CameraPreview.cameraId == Camera.CameraInfo.CAMERA_FACING_BACK){
                    matrix.postRotate(90);
                }
                if(CameraPreview.cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT){
                    matrix.postRotate(-90);
                }
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                data = byteArrayOutputStream.toByteArray();
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                CameraPreview.camera.startPreview();
                broadcastOpenIrisClipping();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    public void takePicture(){
        camera.takePicture(null, null, pictureCallback);
    }

    private void broadcastOpenIrisClipping() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_IRIS_CLIPPING);
        intent.putExtra("path", path);
        getContext().sendBroadcast(intent);
    }

}
