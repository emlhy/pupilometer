package com.example.demonpilot.pupilometer.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;

import java.io.File;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    private Button buttonSingle;
    private Button buttonGallery;
    private Button buttonDual;
    private Button buttonCOWS;
    //private CameraPreview cameraPreview;
    //private Camera camera;
    private String path;
    //private FrameLayout preview;
    //public static File mediaStorageDir;
    public static File file;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        //((ActionBarActivity)getActivity()).getSupportActionBar().hide();
        /*MainActivity.dialog = new ProgressDialog(getActivity());
        MainActivity.dialog.setMessage("Loading...");
        MainActivity.dialog.setCancelable(false);
        MainActivity.dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //camera = getCameraInstance();
        //cameraPreview = new CameraPreview(getActivity(), camera);
        //cameraPreview.setSurfaceTextureListener(cameraPreview);
        //setHasOptionsMenu(true);
        preview = (FrameLayout) rootView.findViewById(R.id.camera_preview);
        //preview.addView(cameraPreview);*/
        buttonSingle = (Button) rootView.findViewById(R.id.single);
        buttonDual = (Button) rootView.findViewById(R.id.dual);
        buttonGallery = (Button) rootView.findViewById(R.id.gallery);
        buttonCOWS = (Button) rootView.findViewById(R.id.cows);

        buttonDual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                /*file = getOutputMediaFile();
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                getActivity().startActivityForResult(intent, 1);*/
                //MainActivity.dialog.show();
                //camera.takePicture(null, null, pictureCallback);
                //photoCrop(uri);
                //broadcastOpenIrisClipping();
                broadcastOpenDual();
            }
        });
        buttonSingle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                broadcastOpenSingle();
            }
        });
        buttonGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.mediaStorageDir.exists() && MainActivity.mediaStorageDir.list().length != 0) {
                    broadcastOpenGallery();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "No image exist.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        buttonCOWS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                broadcastOpenQ1();
            }
        });
        return rootView;
    }

    /*private File getOutputMediaFile(){
        if (! MainActivity.mediaStorageDir.exists()){
            if (! MainActivity.mediaStorageDir.mkdirs()){
                Log.d("Camera Guide", "Required media storage does not exist");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        path = MainActivity.mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg";
        mediaFile = new File(path);

        return mediaFile;
    }

    private void broadcastOpenIrisClipping() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_IRIS_CLIPPING);
        //intent.putExtra("path", path);
        getActivity().sendBroadcast(intent);
    }*/

    private void broadcastOpenDual(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_DUAL);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastOpenSingle(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_SINGLE);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastOpenGallery(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_GALLERY);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastOpenQ1(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_Q1);
        getActivity().sendBroadcast(intent);
    }

    /*private void photoCrop(Uri picUri) {
        Uri imageUri = Uri.parse(mediaStorageDir.getPath() + File.separator + "temp.jpg");
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        //cropIntent.setType("image/*");
        //cropIntent.setAction(Intent.ACTION_GET_CONTENT);
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", 100);
        cropIntent.putExtra("outputY", 100);
        cropIntent.putExtra("scale", true);
        cropIntent.putExtra("return-data", false);
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        cropIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        getActivity().startActivityForResult(cropIntent, 1);
    }

    @Override
    public void onPause() {
        super.onPause();
        preview.removeView(cameraPreview);
    }

    @Override
    public void onResume() {
        super.onResume();
        camera = getCameraInstance();
        cameraPreview = new CameraPreview(getActivity(), camera);
        cameraPreview.setSurfaceTextureListener(cameraPreview);
        preview.addView(cameraPreview);
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {

        }
        return c;
    }

    private Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile();
            if(pictureFile == null) {
                return;
            }
            try{
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                data = byteArrayOutputStream.toByteArray();
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                camera.startPreview();
                //photoCrop(uri);
                broadcastOpenIrisClipping();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };*/
}
