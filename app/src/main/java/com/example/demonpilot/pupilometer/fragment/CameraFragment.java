package com.example.demonpilot.pupilometer.fragment;

import android.app.ProgressDialog;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.demonpilot.pupilometer.preview.CameraPreview;
import com.example.demonpilot.pupilometer.view.FrameView;
import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;

/**
 * Created by Demon Pilot on 2015/10/17.
 */
public class CameraFragment extends Fragment {
    private CameraPreview cameraPreview;
    private FrameLayout preview;
    private FrameLayout frame;
    private Button buttonCapture;
    public static TextView flash;
    public static TextView switchCamera;
    //private String path;
    private Camera.Parameters parameters;
    public static int height;
    public static int width;
    public static int[] position;
    public static int topOffset;
    public boolean hasMeasured;
    private FrameView frameView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_camera, container, false);
        final View view = rootView;
        hasMeasured = false;
        MainActivity.dialog = new ProgressDialog(getActivity());
        MainActivity.dialog.setMessage("Loading...");
        MainActivity.dialog.setCancelable(false);
        MainActivity.dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //camera = getCameraInstance();
        //cameraPreview = new CameraPreview(getActivity(), camera);
        //cameraPreview.setSurfaceTextureListener(cameraPreview);
        preview = (FrameLayout) rootView.findViewById(R.id.camera_preview);
        frame = (FrameLayout) rootView.findViewById(R.id.frame);
        //preview.addView(cameraPreview);
        buttonCapture = (Button)rootView.findViewById(R.id.capture);
        flash = (TextView) rootView.findViewById(R.id.flash);
        switchCamera = (TextView) rootView.findViewById(R.id.switch_camera);

        buttonCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.dialog.show();
                cameraPreview.takePicture();
                //CameraPreview.camera.takePicture(null, null, pictureCallback);
                //broadcastOpenIrisClipping();
            }
        });

        ViewTreeObserver viewTreeobserver = frame.getViewTreeObserver();
        viewTreeobserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if(!hasMeasured) {
                    DisplayMetrics dm = new DisplayMetrics();
                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                    topOffset = dm.heightPixels - view.getMeasuredHeight();
                    height = frame.getHeight();
                    width = frame.getWidth();
                    position = new int[2];
                    frame.getLocationInWindow(position);
                    hasMeasured = true;
                }
                return true;
            }
        });

        frameView = new FrameView(getActivity());
        frameView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        frame.addView(frameView);

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        preview.removeView(cameraPreview);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*CameraPreview.camera = getCameraInstance();
        //parameters = camera.getParameters();
        cameraPreview = new CameraPreview(getActivity(), CameraPreview.camera);
        cameraPreview.setSurfaceTextureListener(cameraPreview);
        preview.addView(cameraPreview);*/
        cameraPreview = new CameraPreview(getActivity());
        cameraPreview.setSurfaceTextureListener(cameraPreview);
        preview.addView(cameraPreview);
    }

    /*private void broadcastOpenIrisClipping() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_IRIS_CLIPPING);
        intent.putExtra("path", path);
        getActivity().sendBroadcast(intent);
    }

    private File getOutputMediaFile(){
        if (! MainActivity.mediaStorageDir.exists()){
            if (! MainActivity.mediaStorageDir.mkdirs()){
                Log.d("Camera Guide", "Required media storage does not exist");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        File mediaFile;
        if(CameraPreview.isFlashOn){
            path = MainActivity.mediaStorageDir.getPath() + File.separator + timeStamp + "_Flash.jpg";
        } else{
            path = MainActivity.mediaStorageDir.getPath() + File.separator + timeStamp + ".jpg";
        }

        mediaFile = new File(path);

        return mediaFile;
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        } catch (Exception e) {

        }
        return c;
    }

    private Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile();
            if(pictureFile == null) {
                return;
            }
            try{
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                Matrix matrix = new Matrix();
                if (CameraPreview.cameraId == Camera.CameraInfo.CAMERA_FACING_BACK){
                    matrix.postRotate(90);
                }
                if(CameraPreview.cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT){
                    matrix.postRotate(-90);
                }
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                data = byteArrayOutputStream.toByteArray();
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                //CameraPreview.camera.startPreview();
                broadcastOpenIrisClipping();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };*/
}
