package com.example.demonpilot.pupilometer.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by Demon Pilot on 2015/9/19.
 */
public class CircleScopeView extends View {
    private Paint circle;
    private DashPathEffect dashPath;
    private Point size;
    private float radiusLeft;
    private float radiusRight;
    public CircleScopeView(Context context) {
        super(context);
    }

    public CircleScopeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleScopeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        dashPath = new DashPathEffect(new float[]{4, 4}, 1);
        circle = new Paint();
        size = new Point();
        circle.setAntiAlias(true);
        circle.setARGB(255, 255, 0, 0);
        circle.setPathEffect(dashPath);
        circle.setStyle(Paint.Style.STROKE);
        circle.setStrokeWidth(4.0f);
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getSize(size);
        canvas.drawCircle(size.x / 2, (int) dpToPx(120), dpToPx(100), circle);
        canvas.drawCircle(size.x / 2, (int) dpToPx(120), dpToPx(getRadiusLeft()), circle);
        canvas.drawCircle(size.x / 2, (int)dpToPx(400), dpToPx(100), circle);
        canvas.drawCircle(size.x / 2, (int)dpToPx(400), dpToPx(getRadiusRight()), circle);
    }

    public void setRadiusLeft(float radius) {
        radiusLeft = radius;
    }

    public float getRadiusLeft() {
        return radiusLeft;
    }

    public void setRadiusRight(float radius) {
        radiusRight = radius;
    }

    public float getRadiusRight() {
        return radiusRight;
    }

    public float dpToPx(float dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        float px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
}
