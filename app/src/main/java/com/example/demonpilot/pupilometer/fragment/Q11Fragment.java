package com.example.demonpilot.pupilometer.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.demonpilot.pupilometer.activity.MainActivity;
import com.example.demonpilot.pupilometer.R;

/**
 * Created by Demon Pilot on 2015/12/9.
 */
public class Q11Fragment extends Fragment {
    private RadioGroup radioGroup;
    private RadioButton radioButton1, radioButton2, radioButton3;
    private Button next;
    private Button cancel;
    private int score;
    private boolean chosen;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_q11, container, false);
        radioGroup = (RadioGroup) rootView.findViewById(R.id.radio_group);
        radioButton1 = (RadioButton) rootView.findViewById(R.id.radio_button_1);
        radioButton2 = (RadioButton) rootView.findViewById(R.id.radio_button_2);
        radioButton3 = (RadioButton) rootView.findViewById(R.id.radio_button_3);
        next = (Button) rootView.findViewById(R.id.next);
        cancel = (Button) rootView.findViewById(R.id.cancel);
        chosen = false;

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == radioButton1.getId()){
                    score = 0;
                    chosen = true;
                } else if(checkedId == radioButton2.getId()){
                    score = 3;
                    chosen = true;
                } else if(checkedId == radioButton3.getId()){
                    score = 5;
                    chosen = true;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chosen) {
                    MainActivity.totalScore += score;
                    broadcastOpenScore();
                } else{
                    Toast.makeText(getActivity(), "Please make a choice first", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Quit COWS")
                        .setMessage("Do you want to quit COWS?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MainActivity.totalScore = 0;
                                broadcastOpenMain();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
            }
        });
        return rootView;
    }

    private void broadcastOpenMain() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_MAIN);
        getActivity().sendBroadcast(intent);
    }

    private void broadcastOpenScore(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_OPEN_SCORE);
        getActivity().sendBroadcast(intent);
    }
}
